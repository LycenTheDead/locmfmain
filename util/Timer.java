package util;

public class Timer {
    private long startTime = System.currentTimeMillis();

    public long getTimeElapsed() {
        return System.currentTimeMillis() - this.startTime;
    }

    public long getSecondsElapsed() {
        return this.getTimeElapsed() / 1000L % 60L;
    }

    public long getMintuesElapsed() {
        return this.getTimeElapsed() / 1000L / 60L % 60L;
    }

    public long getHoursElapsed() {
        return this.getTimeElapsed() / 1000L / 60L / 60L;
    }

    public String getFormattedTime() {
        return Timer.getFormattedTime(this.getTimeElapsed());
    }

    public long calculatePerHour(long gained) {
        return gained * 3600000L / this.getTimeElapsed();
    }

    public static String getFormattedTime(long time) {
        return String.format("%d:%02d:%02d", time / 1000L / 60L / 60L, time / 1000L / 60L % 60L, time / 1000L % 60L);
    }
}