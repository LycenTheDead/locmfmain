package core;

import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.tab.Skill;
import org.rspeer.runetek.api.component.tab.Skills;
import org.rspeer.runetek.api.movement.position.Area;
import org.rspeer.runetek.event.listeners.RenderListener;
import org.rspeer.runetek.event.types.RenderEvent;
import org.rspeer.script.ScriptCategory;
import org.rspeer.script.ScriptMeta;
import org.rspeer.script.task.Task;
import org.rspeer.script.task.TaskScript;
import org.rspeer.ui.Log;
import tasks.manPickpocketing.RobMan;
import tasks.manPickpocketing.WalkToLumbyCourtyard;
import tasks.masterFarmer.PickpocketMF;
import tasks.masterFarmer.WalkToDraynorMarket;
import tasks.teaStall.StealFromStall;
import tasks.teaStall.WalkToTea;
import tasks.sub.Banking;
import tasks.sub.Dropping;
import tasks.sub.EatFood;
import tasks.sub.WalkToBank;
import util.Timer;

import java.awt.*;
import java.text.DecimalFormat;

@ScriptMeta(name = "LOCMF", developer = "Lycen", desc = "Progresses until it can Pickpocket MF for profit!", category = ScriptCategory.MONEY_MAKING, version = 1.0)
public class Main extends TaskScript implements RenderListener {

    private static String statusText = "";
    private static String foodName = "Jug of wine";
    private static boolean shouldStop = false;
    private boolean startScript = true;

    public static final Area LUMBY_COURTYARD = Area.rectangular(3213, 3228, 3226, 3209);
    public static final Area TEA_STALL = Area.rectangular(3269, 3414, 3271, 3411);
    public static final Area DRAYNOR_MARKET = Area.rectangular(3087, 3242, 3073, 3256);
    public static final Area DRAYNOR_BANK = Area.rectangular(3090, 3246, 3095, 3240);

    public Timer t;

    private static int healthPercentThreshold = 60;
    private static int amountOfFood = 12;
    private static int profit = 0;
    private int beginningLevel;
    private int levelsGained;
    private int currentLevel;
    private int beginningXP;
    private int currentXp;
    private int xpGained;




    private final Task[] tasks = {
            new PickpocketMF(),
            new WalkToDraynorMarket(),
            new Banking(),
            new Dropping(),
            new EatFood(),
            new WalkToBank(),
            new StealFromStall(),
            new WalkToTea(),
            new RobMan(),
            new WalkToLumbyCourtyard(),
    };

    @Override
    public void onStart() {
        Log.info("Connected via proxy: "+System.getProperty("socksProxyHost"));
        beginningXP = Skills.getExperience(Skill.THIEVING);
        beginningLevel = Skills.getLevel(Skill.THIEVING);
        t = new Timer();
    }

    @Override
    public int loop() {
        if(!startScript){
            return 0;
        }
        if(!shouldStop) {
            for (Task task : tasks) {
                if (task.validate()) {
                    return task.execute();
                }
            }
        }
        else{
            setStatus("Terminating...");
            Time.sleep(1000);
            this.setStopping(true);
        }
        return 600;
    }

    public static void setProfit(int bankProfit){
        profit = bankProfit;
    }

    public static void setStatus(String status){
        statusText = status;
    }
    public static String getFoodName(){
        return foodName;
    }
    public static int getAmountOfFood(){
        return amountOfFood;
    }

    public static void initiateStop(){
        shouldStop = true;
    }
    public static int getHealthPercentThreshold(){
        return healthPercentThreshold;//ideally, this would be random, but as this is for my level 3's, 60% is the lowest I can do
    }

    @Override
    public void notify(RenderEvent renderEvent) {
        final Color color1 = new Color(36, 36, 36, 160);
        final Color color2 = new Color(0, 0, 0);
        final Color color3 = new Color(255, 255, 255);
        final BasicStroke stroke1 = new BasicStroke(1.0f);
        final Font font1 = new Font("Arial", 0, 11);
        Graphics g = renderEvent.getSource();
        DecimalFormat df = new DecimalFormat("#");
        g.setColor(color1);
        g.fillRect(3, 85, 180, 80);
        g.setColor(color2);
        g.drawRect(3, 85, 180, 80);
        g.setFont(font1);
        g.setColor(color3);

        currentXp = Skills.getExperience(Skill.THIEVING);
        xpGained = currentXp - beginningXP;
        currentLevel = Skills.getLevel(Skill.THIEVING);
        levelsGained = this.currentLevel - this.beginningLevel;

        g.drawString("Time Running: " + this.t.getFormattedTime(), 8, 101);
        g.drawString("Experience [Levels]: " + this.xpGained + "  [" + this.levelsGained + " levels]", 8, 118);
        g.drawString("Profit in bank: "+ profit, 8, 135);
        g.drawString("Status: "+ statusText, 8, 152);
    }
}