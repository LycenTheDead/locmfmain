package tasks.sub;

import core.Main;
import org.rspeer.runetek.adapter.component.Item;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.component.tab.Skill;
import org.rspeer.runetek.api.component.tab.Skills;
import org.rspeer.runetek.api.local.Health;
import org.rspeer.script.task.Task;

import static core.Main.getFoodName;
import static core.Main.getHealthPercentThreshold;
import static core.Main.setStatus;

public class EatFood extends Task {

    @Override
    public int execute() {
        setStatus("Eating");
        Item food = Inventory.getFirst(item -> item.getName().equals(getFoodName()));
        if(food != null){
            food.click();
        }
        Time.sleep(600);
        return 600;
    }

    @Override
    public boolean validate() {
        return Skills.getCurrentLevel(Skill.THIEVING) >= 38 && !Inventory.isFull() && Inventory.contains(getFoodName()) && Health.getPercent() < getHealthPercentThreshold();
    }


}
