package tasks.sub;

import core.Main;
import org.rspeer.runetek.adapter.component.Item;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.script.task.Task;

import static core.Main.getFoodName;
import static core.Main.setStatus;

public class Dropping extends Task {
    @Override
    public int execute() {
        setStatus("Dropping uneccesarry items");
        for (Item item : Inventory.getItems(item -> !item.getName().equals(getFoodName()) && !item.getName().equals("Snapdragon seed") && !item.getName().equals("Torstol seed") && !item.getName().equals("Snape grass seed") && !item.getName().equals("Toadflax seed"))) {
            item.interact("Drop");
            Time.sleep(400);
        }
        return 600;
    }

    @Override
    public boolean validate() {
        return Inventory.isFull();
    }
}