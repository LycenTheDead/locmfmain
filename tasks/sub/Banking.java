package tasks.sub;

import core.Main;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.Bank;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.component.tab.Skill;
import org.rspeer.runetek.api.component.tab.Skills;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.script.task.Task;

import static core.Main.*;
import static org.rspeer.runetek.api.commons.Time.sleepUntil;

public class Banking extends Task {
    int bankProfit = 0;

    @Override
    public int execute() {
        setStatus("Banking");
        draynorBank();
        Time.sleep(200, 600);
        return 600;
    }

    @Override
    public boolean validate() {
        return  Skills.getCurrentLevel(Skill.THIEVING) >= 38 && !Inventory.isFull() && !Inventory.contains(getFoodName()) && Main.DRAYNOR_BANK.contains(Players.getLocal());
    }

    public void draynorBank(){
            if(Bank.isOpen()) {
                Bank.depositInventory();
                sleepUntil((()-> Inventory.isEmpty()), 600);

                bankProfit = Bank.getCount("Ranarr seed") * 39498;
                bankProfit += Bank.getCount("Toadflax seed") * 399;
                bankProfit += Bank.getCount("Torstol seed") * 57015;
                bankProfit += Bank.getCount("Snapdragon seed") * 51780;
                Main.setProfit(bankProfit);
                String tempFoodName = getFoodName();
                if (Bank.contains(tempFoodName)) {
                    if (!Inventory.contains(tempFoodName) && !Inventory.isFull()) {
                        Bank.withdraw(tempFoodName, getAmountOfFood());
                        sleepUntil((()-> Inventory.contains(tempFoodName)), 600);
                        Bank.close();
                        sleepUntil((()-> Bank.isClosed()), 600);
                    }
                }
                else{
                    Bank.close();
                    initiateStop();
                }
            }

            else {
                Bank.open();
                sleepUntil((()-> Bank.isOpen()), 600);
            }
    }
}
