package tasks.sub;

import core.Main;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.component.tab.Skill;
import org.rspeer.runetek.api.component.tab.Skills;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.script.task.Task;

import static core.Main.getFoodName;
import static core.Main.setStatus;

public class WalkToBank extends Task {
    @Override
    public int execute() {
        if (!Movement.isRunEnabled() && Movement.getRunEnergy() > 25) {
            setStatus("Enabling run");
            Movement.toggleRun(true);
        }
        setStatus("Walking to Draynor Bank");
        Movement.walkTo(Main.DRAYNOR_BANK.getCenter());
        Time.sleep(700, 1200);
        return 600;
    }

    @Override
    public boolean validate() {
        return Skills.getCurrentLevel(Skill.THIEVING) >= 38 && !Inventory.isFull() && !Inventory.contains(getFoodName()) && !Main.DRAYNOR_BANK.contains(Players.getLocal());
    }
}