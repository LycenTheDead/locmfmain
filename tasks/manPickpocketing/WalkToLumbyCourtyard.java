package tasks.manPickpocketing;

import core.Main;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.script.task.Task;

import static core.Main.setStatus;

public class WalkToLumbyCourtyard extends Task {

    @Override
    public int execute() {
        if (!Movement.isRunEnabled() && Movement.getRunEnergy() > 25) {
            setStatus("Enabling run");
            Movement.toggleRun(true);
        }
        setStatus("Walking to Lumbridge Courtyard");
        Movement.walkTo(Main.LUMBY_COURTYARD.getCenter());
        Time.sleep(700, 1200);
        return 600;
    }

    @Override
    public boolean validate() {
        return !Main.LUMBY_COURTYARD.contains(Players.getLocal()) && !Inventory.isFull();
    }
}