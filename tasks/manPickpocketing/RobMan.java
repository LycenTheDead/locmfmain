package tasks.manPickpocketing;

import core.Main;
import org.rspeer.runetek.adapter.component.Item;
import org.rspeer.runetek.adapter.scene.Npc;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.scene.Npcs;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

import static core.Main.setStatus;

public class RobMan extends Task {
    @Override
    public int execute() {
        if(Inventory.contains("Coin pouch")){
            Log.info("Opening coin pouches");
            Item coinPouch = Inventory.getFirst(item -> item.getName().equals("Coin pouch"));
            coinPouch.interact("Open-all");
        }
        pickpocketMan();
        return 600;
    }

    @Override
    public boolean validate() {
        return Main.LUMBY_COURTYARD.contains(Players.getLocal()) && !Inventory.isFull();
    }

    public void pickpocketMan() {
        setStatus("Pickpocketing Man");
        Npc man = Npcs.getNearest(npc -> npc.getName().equals("Man"));

        if (!Players.getLocal().isAnimating() && man != null) {
            man.interact("Pickpocket");
            Time.sleep(100);
        }
    }
}