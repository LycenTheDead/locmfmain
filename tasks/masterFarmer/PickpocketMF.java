package tasks.masterFarmer;

import core.Main;
import org.rspeer.runetek.adapter.scene.Npc;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.component.tab.Skill;
import org.rspeer.runetek.api.component.tab.Skills;
import org.rspeer.runetek.api.local.Health;
import org.rspeer.runetek.api.scene.Npcs;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.script.task.Task;

import static core.Main.getFoodName;
import static core.Main.getHealthPercentThreshold;
import static core.Main.setStatus;

public class PickpocketMF extends Task {

    @Override
    public int execute() {
        setStatus("Pickpocketing Master Farmer");
        pickpocketFarmer();
        return 600;
    }

    @Override
    public boolean validate() {
        return Main.DRAYNOR_MARKET.contains(Players.getLocal()) && Skills.getCurrentLevel(Skill.THIEVING) >= 38 && !Inventory.isFull() &&
                Inventory.contains(getFoodName()) && Health.getPercent() >= getHealthPercentThreshold();
    }

    public void pickpocketFarmer() {
        if (Inventory.isItemSelected()) {
            Inventory.deselectItem();
            Time.sleep(400);
        }
        Npc farmer = Npcs.getNearest(npc -> npc.getName().equals("Master Farmer"));

        if (!Players.getLocal().isAnimating() && farmer != null) {
            farmer.interact("Pickpocket");
            Time.sleep(100);
        }
    }

}
