package tasks.masterFarmer;

import core.Main;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.component.tab.Skill;
import org.rspeer.runetek.api.component.tab.Skills;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.script.task.Task;

import static core.Main.getFoodName;
import static core.Main.setStatus;

public class WalkToDraynorMarket extends Task {

    @Override
    public int execute() {
        if (!Movement.isRunEnabled() && Movement.getRunEnergy() > 25) {
            setStatus("Enabling run");
            Movement.toggleRun(true);
        }
        setStatus("Walking to Draynor Market");
        Movement.walkTo(Main.DRAYNOR_MARKET.getCenter());
        Time.sleep(700, 1200);
        return 600;
    }

    @Override
    public boolean validate() {
        return !Main.DRAYNOR_MARKET.contains(Players.getLocal()) && Skills.getCurrentLevel(Skill.THIEVING) >= 38 && !Inventory.isFull() && Inventory.contains(getFoodName());
    }
}