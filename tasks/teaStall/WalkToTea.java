package tasks.teaStall;

import core.Main;
import org.rspeer.runetek.adapter.component.Item;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.component.tab.Skill;
import org.rspeer.runetek.api.component.tab.Skills;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

import static core.Main.setStatus;

public class WalkToTea extends Task {

    @Override
    public int execute() {
        if (!Movement.isRunEnabled() && Movement.getRunEnergy() > 25) {
            setStatus("Enabling run");
            Movement.toggleRun(true);
        }
        if(Inventory.contains("Coin pouch")){
            Log.info("Opening coin pouches");
            Item coinPouch = Inventory.getFirst(item -> item.getName().equals("Coin pouch"));
            coinPouch.interact("Open-all");
        }
        setStatus("Walking to Tea Stall");
        Movement.walkTo(Main.TEA_STALL.getCenter());
        Time.sleep(700, 1200);

        return 600;
    }

    @Override
    public boolean validate() {
        return !Main.TEA_STALL.contains(Players.getLocal()) && Skills.getCurrentLevel(Skill.THIEVING) >= 5 && !Inventory.isFull();
    }
}