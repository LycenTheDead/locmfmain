package tasks.teaStall;

import core.Main;
import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.component.tab.Skill;
import org.rspeer.runetek.api.component.tab.Skills;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.runetek.api.scene.SceneObjects;
import org.rspeer.script.task.Task;

import static core.Main.setStatus;

public class StealFromStall extends Task {
    @Override
    public int execute() {
        setStatus("Stealing Tea");
        SceneObject tea_stall = SceneObjects.getNearest("Tea Stall");
        if(tea_stall != null) {
            tea_stall.interact("Steal-from");
            Time.sleep(300, 500);
        }
        return 600;
    }

    @Override
    public boolean validate() {
        return Main.TEA_STALL.contains(Players.getLocal()) && Skills.getCurrentLevel(Skill.THIEVING) >= 5 && !Inventory.isFull();
    }
}